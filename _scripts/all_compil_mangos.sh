#!/bin/bash

rm -rf build
echo 'Build folder has been removed'
mkdir build
cd build
cmake -DPCH=ON ./..
make
make install

echo 'make job done'

cd /mnt/mangos-tbc/opt/cmangos/bin
rm -rf mangosd
mv /usr/local/bin/mangosd .

echo 'mangosd has been updated'
