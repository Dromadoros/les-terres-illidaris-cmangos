#!/bin/bash

cd build
make
make install
cd /mnt/mangos-tbc/opt/cmangos/bin
rm -rf mangosd
cp /usr/local/bin/mangosd .
